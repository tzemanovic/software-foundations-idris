namespace Booleans

  ||| Boolean Data Type
  data Bool = True | False

  not : (b : Bool) -> Bool
  not True  = False
  not False = True

  andb : (b1 : Bool) -> (b2 : Bool) -> Bool
  andb True b2  = b2
  andb False b2 = False

  orb : (b1 : Bool) -> (b2 : Bool) -> Bool
  orb True b2  = True
  orb False b2 = b2

  nandb : (b1 : Bool) -> (b2 : Bool) -> Bool
  nandb False b2 = True
  nandb True b2  = not b2

  infixl 4 &&, ||

  (&&) : Bool -> Bool -> Bool
  (&&) = andb

  (||) : Bool -> Bool -> Bool
  (||) = orb

  andb3 : (b1 : Bool) -> (b2 : Bool) -> (b3 : Bool) -> Bool
  andb3 b1 b2 b3 = b1 && b2 && b3

  test_orb1 : (orb True False) = True
  test_orb1 = Refl

  test_orb2 : (orb False False) = False
  test_orb2 = Refl

  test_orb3 : (orb False True) = True
  test_orb3 = Refl

  test_orb4 : (orb True True) = True
  test_orb4 = Refl

  test_orb5 : False || False || True = True
  test_orb5 = Refl

  test_nandb1 : (nandb True False) = True
  test_nandb1 = Refl

  test_nandb2 : (nandb False False) = True
  test_nandb2 = Refl

  test_nandb3 : (nandb False True) = True
  test_nandb3 = Refl

  test_nandb4 : (nandb True True) = False
  test_nandb4 = Refl

  test_andb31 : (andb3 True True True) = True
  test_andb31 = Refl

  test_andb32 : (andb3 False True True) = False
  test_andb32 = Refl

  test_andb33 : (andb3 True False True) = False
  test_andb33 = Refl

  test_andb34 : (andb3 True True False) = False
  test_andb34 = Refl

  andb_true_elim_2 : (b, c : Bool) -> (b && c = True) -> c = True
  andb_true_elim_2 True c prf = rewrite prf in Refl
  andb_true_elim_2 b True prf = Refl

  identity_fn_applied_twice : (f : Bool -> Bool) ->
                              ((x : Bool) -> f x = x) ->
                              (b : Bool) -> f (f b) = b
  identity_fn_applied_twice f g b =
    rewrite (g b) in
    rewrite (g b) in Refl

  test_not1 : (a : Bool) -> not (not a) = a
  test_not1 True  = Refl
  test_not1 False = Refl

  negation_fn_applied_twice : (f : Bool -> Bool) ->
                              ((x : Bool) -> f x = not x) ->
                              (b : Bool) -> f (f b) = b
  negation_fn_applied_twice f g b =
    rewrite (g b) in
    rewrite (g (not b)) in test_not1 b

  total andb_eq_orb : (b, c : Bool) -> (b && c = b || c) -> b = c
  andb_eq_orb True True _    = Refl
  andb_eq_orb True False prf = sym prf
  andb_eq_orb False True prf = prf
  andb_eq_orb False False _  = Refl
