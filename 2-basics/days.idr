namespace Days

  ||| Days of the week.
  data Day = ||| `Monday` is a `Day`.
             Monday
           | ||| `Tuesday` is a `Day`.
             Tuesday
           | ||| `Wednesday` is a `Day`.
             Wednesday
           | ||| `Thursday` is a `Day`.
             Thursday
           | ||| `Friday` is a `Day`.
             Friday
           | ||| `Saturday` is a `Day`.
             Saturday
           | ||| `Sunday` is a `Day`.
             Sunday

  %name Day day, day1, day2

  nextWeekday : Day -> Day
  nextWeekday Monday    = Tuesday
  nextWeekday Tuesday   = Wednesday
  nextWeekday Wednesday = Thursday
  nextWeekday Thursday  = Friday
  nextWeekday Friday    = Saturday
  nextWeekday Saturday  = Sunday
  nextWeekday Sunday    = Monday

  ||| The second weekday after `Saturday` is `Tuesday`.
  testNextWeekday : (nextWeekday (nextWeekday Saturday)) = Monday
  testNextWeekday = Refl
