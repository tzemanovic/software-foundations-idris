namespace Numbers

  data Nat : Type where
         Z : Nat
         S : Nat -> Nat

  plus : (n : Nat) -> (m : Nat) -> Nat
  plus Z m     = m
  plus (S k) m = S (plus k m)

  mult : (n, m : Nat) -> Nat
  mult Z     _ = Z
  mult (S k) m = plus m (mult k m)

  -- cannot get Nat of Integer without prelude...
  three : Nat
  three = S (S (S Z))

  nine : Nat
  nine = S (S (S (S (S (S (S (S (S Z))))))))

  test_mult1 : (mult Main.Numbers.three Main.Numbers.three) = Main.Numbers.nine
  test_mult1 = Refl

  factorial : (n : Nat) -> Nat
  factorial Z     = S Z
  factorial (S k) = mult (S k) (factorial k)

  six : Nat
  six = S (S (S (S (S (S Z)))))

  test_factorial1 : factorial Main.Numbers.three = Main.Numbers.six
  test_factorial1 = Refl

  data Bool = True | False

  ||| Test whether a number is less than or equal to another.
  lte : (n, m : Nat) -> Bool
  lte Z _         = True
  lte _ Z         = False
  lte (S k) (S j) = lte k j

  two : Nat
  two = S (S Z)

  four : Nat
  four = S three

  five : Nat
  five = S four

  test_lte1 : lte Main.Numbers.two Main.Numbers.two = True
  test_lte1 = Refl

  test_lte2 : lte Main.Numbers.two Main.Numbers.four = True
  test_lte2 = Refl

  test_lte3 : lte Main.Numbers.four Main.Numbers.two = False
  test_lte3 = Refl

  blt_nat : (n, m : Nat) -> Bool
  blt_nat _ Z     = False
  blt_nat n (S k) = lte n k

  test_blt_nat_1 : blt_nat Main.Numbers.two Main.Numbers.two = False
  test_blt_nat_1 = Refl

  test_blt_nat_2 : blt_nat Main.Numbers.two Main.Numbers.four = True
  test_blt_nat_2 = Refl

  test_blt_nat_3 : blt_nat Main.Numbers.four Main.Numbers.two = False
  test_blt_nat_3 = Refl

  infixl 4 +

  (+) : (n : Nat) -> (m : Nat) -> Nat
  (+) = plus

  plus_id_exercise : (n, m, o : Nat) -> (n = m) -> (m = o) -> n + m = m + o
  plus_id_exercise n m o prf prf1 = rewrite prf in rewrite prf1 in Refl

  infixl 4 *

  (*) : (n : Nat) -> (m : Nat) -> Nat
  (*) = mult

  mult_S_1 : (n, m : Nat) -> (m = S n) -> m * ((S Z) + n) = m * m
  mult_S_1 n m prf = rewrite prf in Refl

  infixl 1 ==

  (==) : (a : Nat) -> (b : Nat) -> Bool
  Z     == Z     = True
  (S j) == (S k) = j == k
  _     == _     = False

  zero_nbeq_plus_1 : (n : Nat) -> Z == (n + (S Z)) = False
  zero_nbeq_plus_1 Z     = Refl
  zero_nbeq_plus_1 (S Z) = Refl

  data Bin : Type where
         O : Bin        -- 0
         T : Bin -> Bin -- 2 * n
         P : Bin -> Bin -- 2 * n + 1

  %name Bin b, b1, b2

  incr : Bin -> Bin
  incr O = P O            -- zero
  incr (T b) = P b        -- twice a binary number
  -- 2 * b + 1 + 1 = 2 * (b + 1)
  incr (P b) = T (incr b) -- one more than twice a binary number

  bin_to_nat : Bin -> Nat
  bin_to_nat O     = Z
  bin_to_nat (T b) = two * (bin_to_nat b)
  bin_to_nat (P b) = two * (bin_to_nat b) + (S Z)

  test_plus_neutral : (a : Nat) -> a + Z = a
  test_plus_neutral Z     = Refl
  test_plus_neutral (S x) = rewrite test_plus_neutral x in Refl

  test_plus_one : (a : Nat) -> S a = a + S Z
  test_plus_one Z     = Refl
  test_plus_one (S x) = rewrite test_plus_one x in Refl

  test_plus_succ_right : (a, b : Nat) -> S (a + b) = a + S b
  test_plus_succ_right Z b     = Refl
  test_plus_succ_right (S x) b = rewrite test_plus_succ_right x b in Refl

  test_plus_commutes : (a, b : Nat) -> a + b = b + a
  test_plus_commutes Z b = rewrite test_plus_neutral b in Refl
  test_plus_commutes (S x) b =
    rewrite test_plus_commutes x b in
    rewrite test_plus_succ_right b x in Refl

  test_bin_incr1 : bin_to_nat (incr O) = S Z
  test_bin_incr1 = Refl

  test_bin_incr2 : bin_to_nat (incr (P O)) = Main.Numbers.two
  test_bin_incr2 = Refl

  test_bin_incr3 : bin_to_nat (incr (T (P O))) = Main.Numbers.three
  test_bin_incr3 = Refl

  test_bin_incr4 : bin_to_nat (incr (P (P O))) = Main.Numbers.four
  test_bin_incr4 = Refl

  test_bin_incr5 : bin_to_nat (incr (T (T (P O)))) = Main.Numbers.five
  test_bin_incr5 = Refl
