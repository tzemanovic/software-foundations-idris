module Induction

import Prelude.Interfaces
import Prelude.Nat

%access public export
%default total

plus_n_Z : (n : Nat) -> n = n + 0
plus_n_Z Z     = Refl
plus_n_Z (S k) =
  let hypo = plus_n_Z k in
  rewrite hypo in Refl

minus_diag : (n : Nat) -> minus n n = 0
minus_diag Z     = Refl
minus_diag (S k) = minus_diag k

mult_0_r : (n : Nat) -> n * 0 = 0
mult_0_r Z     = Refl
mult_0_r (S k) = mult_0_r k

plus_n_Sm : (n, m : Nat) -> S (n + m) = n + (S m)
plus_n_Sm Z m     = Refl
plus_n_Sm (S k) m = rewrite plus_n_Sm k m in Refl

plus_comm : (n, m : Nat) -> n + m = m + n
plus_comm Z m     = rewrite plus_n_Z m in Refl
plus_comm (S k) m =
  rewrite plus_comm k m in
  rewrite plus_n_Sm m k in Refl

plus_assoc : (n, m, p : Nat) -> n + (m + p) = (n + m) + p
plus_assoc Z m p     = Refl
plus_assoc (S k) m p =
  let hypo = plus_assoc k m p in
  rewrite hypo in Refl

double : (n : Nat) -> Nat
double Z  = Z
double (S k) = S (S (double k))

double_plus : (n : Nat) -> double n = n + n
double_plus Z     = Refl
double_plus (S k) =
  rewrite double_plus k in
  rewrite plus_n_Sm k k in Refl

evenb : (n : Nat) -> Bool
evenb Z     = True
evenb (S k) = not (evenb k)

evenb_S : (n : Nat) -> evenb (S n) = not (evenb n)
evenb_S n = Refl

plus_swap : (n, m, p : Nat) -> n + (m + p) = m + (n + p)
plus_swap n m p =
  rewrite plus_assoc n m p in
  rewrite plus_assoc m n p in
  rewrite plus_comm n m in Refl

mult_comm : (n, m : Nat) -> n * m = m * n
mult_comm Z Z = Refl
mult_comm Z (S k) = rewrite mult_comm Z k in Refl
mult_comm (S k) j =
  let hypo = mult_comm k j in
  rewrite hypo in
  rewrite lemma j k in Refl
  where
  lemma : (n, m : Nat) -> n * (S m) = n + (n * m)
  lemma Z m = Refl
  lemma (S i) m =
    rewrite lemma i m in
    rewrite plus_assoc i m (i * m) in
    rewrite plus_assoc m i (i * m) in
    rewrite plus_comm i m in Refl
